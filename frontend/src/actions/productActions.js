import Axios from "axios";

const { PRODUCT_LIST_REQUEST, PRODUCT_LIST_SUCCESS, PRODUCT_LIST_FAILED } = require("../constants/productConstants")

const listProducts = () => async (dispatch) =>{
    try{
    dispatch({type: PRODUCT_LIST_REQUEST});
    const{data} = await Axios.get("/api/products");
    dispatch({type:PRODUCT_LIST_SUCCESS,payload:data});
    }
    catch(error){
        dispatch({type:PRODUCT_LIST_FAILED, payload: error.message});
    }

}

export {listProducts} 