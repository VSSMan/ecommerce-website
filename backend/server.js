import express from 'express';
import data from './data';

var cors = require('cors')
const app = express();
app.use(cors());


app.get("/api/products", (req, res) => {
  res.send(data.products);
});

app.listen(5000, () => { console.log("Server started at http://localhost:5000") });
